$(function() {
  // Vars
  var vary = "header",
    typed = new Typed(".name", {
      strings: [
        '<span class="first">BA</span>SEM 3SALYA',
        '<span class="first">A P</span>rofessional Freelancer'
      ],
      typeSpeed: 200,
      backSpeed: 100,
      smartBackspace: true,
      loop: true,
      loopCount: Infinity,
      showCursor: false,
      contentType: "html"
    });

  // loading Effect
  $(window).load(function() {
    $(this).scrollTop(0);
    $(".se-pre-con").fadeOut(2000);
  });
  window.onbeforeunload = function() {
    window.scrollTo(0, 0);
  };

  // Hide Nav
  $(".navbar-list .hidden-nav").click(function() {
    $("nav").animate({ left: "-100px" }, 100, function() {
      $(".show-nav").animate({ left: "0px" }, 100);
    });
    $("header , footer, section").width("100%");
  });
  $(".show-nav").click(function() {
    $("header , footer, section").width("calc(100% - 80px)");
    $(".show-nav").animate({ left: "-100px" }, 100, function() {
      $("nav").animate({ left: "0px" }, 100);
    });
  });

  // Navgation Scroll & overs
  $("nav .navbar-list li a").click(function(e) {
    if ($(this).data("go") !== "none") {
      e.preventDefault();
      if (vary !== $(this).data("go")) {
        $(".curtain").css("visibility", "visible");
        $(".curtain .ov-hed .over").animate({ left: "0" }, 500);
        $("html, body")
          .delay(500)
          .animate({ scrollTop: $("#" + $(this).data("go")).offset().top }, 10);
        $(".curtain .ov-hed .over")
          .delay(500)
          .animate({ left: "100%" }, 600, function() {
            $(".curtain").css("visibility", "hidden");
          });
        vary = $(this).data("go");
      }
    }
  });

  // Sections Hight
  if ($(window).width() > 768) {
    $("header, nav, section, footer").height($(window).height());
  } else {
    $("header, section, footer").height($(window).height());
    $("nav").height("auto");
  }

  // Resize
  $(window).resize(function() {
    if ($(window).width() > 768) {
      $("header, nav, section, footer").height($(window).height());
    } else {
      $("header, section, footer").height($(window).height());
      $("nav").height("auto");
    }
    $("html, body").animate({ scrollTop: $("#" + vary).offset().top }, 10);
  });

  // typed.js plugin
  typed = new Typed(".text-type", {
    strings: [
      "an expert in HTML5, CSS3 and JavaScript",
      "I have worked more than these languages.",
      "also a developer of WordPress for three years and have a lot of projects online."
    ],
    typeSpeed: 100,
    loop: true,
    loopCount: "Infinity"
  });
});
