const { src, dest, series, parallel, watch } = require("gulp"),
  htmlval = require("gulp-w3cjs"),
  imagemin = require("gulp-imagemin"),
  jsmin = require("gulp-uglify"),
  cssmin = require("gulp-csso"),
  concat = require("gulp-concat"),
  rename = require("gulp-rename");
const html = () => {
    return src("src/*.html").pipe(htmlval.reporter()).pipe(dest("dist"));
  },
  css = () => {
    return src([
      "./src/css/font-awesome.css",
      "./src/css/base.css",
      "./src/css/layout.css",
      "./src/css/module.css",
      "./src/css/state.css",
      "./src/css/theme.css",
    ])
      .pipe(concat("all.css"))
      .pipe(cssmin())
      .pipe(rename({ basename: "main" }))
      .pipe(dest("dist/css/"));
  },
  js = () => {
    return src(["./src/js/jquery.js", "./src/js/typed.js", "./src/js/main.js"])
      .pipe(concat("all.js"))
      .pipe(jsmin())
      .pipe(rename({ basename: "main" }))
      .pipe(dest("dist/js/"));
  },
  img = () => {
    return src("src/images/*").pipe(imagemin()).pipe(dest("dist/img/"));
  },
  fonts = () => {
    return src("./src/fonts/*").pipe(dest("./dist/fonts/"));
  };
watcher = () => {
  watch("src/**/*", parallel(html, css, js, img, fonts));
};

exports.build = parallel(html, css, js, img, fonts);
module.exports = {
  default: watcher,
  build: parallel(html, css, js, img, fonts),
  html,
  css,
  js,
  img,
  fonts,
};
