# 3salya Resume template

HTML5, CSS3 and jQuery Fully Responsive Easy to Customizable W3C Validated Code Minimal and Clean Free Google Fonts. All files are well commented Displays well in all modern browsers and devices

![Preview Img](UX/screen.png)

For Live Preview :https://3salya-resume.mohamed-ashamallah.com/

## Quickstart

1. Install the [node.js](https://nodejs.org/en/)
2. Clone the project

   ```bash
   git clone https://Power50015@bitbucket.org/Power50015/3salya-resume.git
   ```

3. Go to project folder and run

   ```bash
   npm install
   ```

   ```bash
   npm install --global gulp-cli
   ```

4. Start development mode

   ```bash
   gulp
   ```

5. Open index.html in dist file

### Main tasks

- gulp - launches watchers and server & compile project.
- gulp build - optimize & minify files for production version.

## INTRODUCTION & FEATURE

Creative & Modern One Page Template is a perfect template for Profile, web
studio and creative agencies. This is one page for placing your
information. All files and code has been well organized and nicely commented for easy to customize.

## MAIN FEATURES :

- Valid HTML5, CSS3, jQuery.
- W3C Validated.
- HTML5, CSS3, jQuery.
- Fully Customizable.
- Clean Code.
- Fully Responsive.
- Font-awesome-4.7.0.

## FILES INCLUDED :

- HTML Files.
- CSS Files.
- JS Files.
- Gulp Files.

## Credits

- Google Fonts
- Font Awesome
- Jquery
- Type.js
- Gulp

## Support:

- If you need any help using the file or need special customizing please contact me via my Bitbucket or my Website.
- If you like my html template, please follwo me , We’ll appreciate it very much Thank you.
